﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using RDotNet;
using System.Diagnostics;
using System.Threading;

namespace SimulationUI
{
    public partial class Form1 : Form
    {
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int T_value(double m, double d, int N, double p);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void U_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, double max_p, int tests, double eps,double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void N_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_p, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void U_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, int max_N, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int r_value(int N, double p);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void U_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, double max_m, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void U_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, double max_d, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void P_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, int max_N, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void P_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, double max_p, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void P_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, double max_m, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void P_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, double max_d, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void N_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_m, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void N_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_d, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void p_and_m_variation(double m, double d, int N, double p, int L, int U, int P, double max_p, double max_m, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void p_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double max_p, double max_d, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void m_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double max_m, double max_d, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void U_and_P_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, int max_P, int tests, double eps, double p_threshold);
        [DllImport("../../Simulation/Simulation.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double bankruptcy_probability(double m, double d, int N, double p, int L, int U, int P, double eps);

        public double m, d, p, eps, max_p,max_m,max_d, p_threshold;
        public int N, L, U, P, max_U, max_N,max_P, tests;

        private void pTextBox_TextChanged(object sender, EventArgs e)
        {
            if (pTextBox.Text.Length != 0 && double.TryParse(pTextBox.Text, out p)&& Convert.ToDouble(pTextBox.Text)<=1)
            {
                p = Convert.ToDouble(pTextBox.Text);
                if (max_p < p && maxMTextBox.Text != "")
                {
                    maxPTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxPTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                pTextBox.Text = "";
            }
        }

        private void UTextBox_TextChanged(object sender, EventArgs e)
        {
            if (UTextBox.Text.Length != 0 && int.TryParse(UTextBox.Text, out U))
            {
                U = Convert.ToInt32(UTextBox.Text);
                if (max_U < U && maxMTextBox.Text != "")
                {
                    maxUTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxUTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                UTextBox.Text = "";
            }
        }
        
        private void bigPTextBox_TextChanged(object sender, EventArgs e)
        {
            if (bigPTextBox.Text.Length != 0 && int.TryParse(bigPTextBox.Text, out P))
            {
                P = Convert.ToInt32(bigPTextBox.Text);
                if (max_P < P && maxMTextBox.Text != "")
                {
                    maxBigPTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxBigPTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                bigPTextBox.Text = "";
            }
        }

        private void maxNTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxNTextBox.Text.Length != 0 && int.TryParse(maxNTextBox.Text, out max_N))
            {
                max_N = Convert.ToInt32(maxNTextBox.Text);
                if (max_N < N)
                {
                    maxNTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxNTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                maxNTextBox.Text = "";
            }
            if (maxNTextBox.Text == "")
            {
                maxNTextBox.BackColor = SystemColors.InactiveCaption;
            }
            
        }

        private void maxPTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxPTextBox.Text.Length != 0 && double.TryParse(maxPTextBox.Text, out max_p)&& Convert.ToDouble(maxPTextBox.Text)<=1)
            {
                max_p = Convert.ToDouble(maxPTextBox.Text);
                if (max_p < p)
                {
                    maxPTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxPTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                maxPTextBox.Text = "";
            }
            if (maxPTextBox.Text == "")
            {
                maxPTextBox.BackColor = SystemColors.InactiveCaption;
            }
            
        }

        private void maxUTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxUTextBox.Text.Length != 0 && int.TryParse(maxUTextBox.Text, out max_U))
            {
                max_U = Convert.ToInt32(maxUTextBox.Text);
                if (max_U < U)
                {
                    maxUTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxUTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                maxUTextBox.Text = "";
            }
            if (maxUTextBox.Text == "")
            {
                maxUTextBox.BackColor = SystemColors.InactiveCaption;
            }
            
        }

        private void testsTextBox_TextChanged(object sender, EventArgs e)
        {
            if (testsTextBox.Text.Length != 0 && int.TryParse(testsTextBox.Text, out tests))
            {
                tests = Convert.ToInt32(testsTextBox.Text);
            }
            else
            {
                testsTextBox.Text = "";
            }
        }

        private void maxBigPTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxBigPTextBox.Text.Length != 0 && int.TryParse(maxBigPTextBox.Text, out max_P))
            {
                max_P = Convert.ToInt32(maxBigPTextBox.Text);
                if (max_P < P)
                {
                    maxBigPTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxBigPTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                maxBigPTextBox.Text = "";
            }
            if (maxMTextBox.Text == "")
            {
                maxBigPTextBox.BackColor = SystemColors.InactiveCaption;
            }
        }

        private void maxMTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxMTextBox.Text.Length != 0 && double.TryParse(maxMTextBox.Text, out max_m))
            {
                max_m = Convert.ToDouble(maxMTextBox.Text);
                if(max_m<m)
                {
                    maxMTextBox.BackColor = Color.Red;
                }
                else 
                {
                    maxMTextBox.BackColor = SystemColors.InactiveCaption;
                }    
            }
            else
            {
                maxMTextBox.Text = "";
            }
            if (maxMTextBox.Text == "")
            {
                maxMTextBox.BackColor = SystemColors.InactiveCaption;
            }
        }

        private void UP()
        {
            U_and_P_variation(m, d, N, p, L, U, P, max_U, max_P, tests, eps, p_threshold);
        }
        private async void UandBigPButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxUTextBox.Text != "" && maxBigPTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => UP());
                //after calculating data
                populateGrid("U", "P");
                progressBar1.Hide();
            }
        }

        private void Um()
        {
            U_and_m_variation(m, d, N, p, L, U, P, max_U, max_m, tests, eps, p_threshold);
        }
        private async void UandMButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxUTextBox.Text != "" && maxMTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Um());
                //after calculating data
                populateGrid("U", "m");
                progressBar1.Hide();
            }
        }

        private void Ud()
        {
            U_and_d_variation(m, d, N, p, L, U, P, max_U, max_d, tests, eps, p_threshold);
        }
        private async void UandDButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxUTextBox.Text != "" && maxDTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Ud());
                //after calculating data
                populateGrid("U", "d");
                progressBar1.Hide();
            }
        }

        private void Nm()
        {
            N_and_m_variation(m, d, N, p, L, U, P, max_N, max_m, tests, eps, p_threshold);
        }
        private async void NandMButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxNTextBox.Text != "" && maxMTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Nm());
                //after calculating data
                populateGrid("N", "m");
                progressBar1.Hide();
            }
        }

        private void Nd()
        {
            N_and_d_variation(m, d, N, p, L, U, P, max_N, max_d, tests, eps, p_threshold);
        }
        private async void NandDButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxNTextBox.Text != "" && maxDTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Nd());
                //after calculating data
                populateGrid("N", "d");
                progressBar1.Hide();
            }
        }

        private void NP()
        {
            P_and_N_variation(m, d, N, p, L, U, P, max_P, max_N, tests, eps,p_threshold);
        }
        private async void NandBigPButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxNTextBox.Text != "" && maxBigPTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => NP());
                //after calculating data
                populateGrid("N", "P");
                progressBar1.Hide();
            }
        }

        private void Pp()
        {
            P_and_p_variation(m, d, N, p, L, U, P, max_P, max_p, tests, eps, p_threshold);
        }
        private async void PandPButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxBigPTextBox.Text != "" && maxPTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Pp());
                //after calculating data
                populateGrid("P", "p");
                progressBar1.Hide();
            }
        }

        private void Pm()
        {
            P_and_m_variation(m, d, N, p, L, U, P, max_P, max_m, tests, eps, p_threshold);
        }
        private async void BigPandMButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxBigPTextBox.Text != "" && maxMTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Pm());
                //after calculating data
                populateGrid("P", "m");
                progressBar1.Hide();
            }
        }

        private void md()
        {
            m_and_d_variation(m, d, N, p, L, U, P, max_m, max_d, tests, eps, p_threshold);
        }
        private async void MandDButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxMTextBox.Text != "" && maxDTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => md());
                //after calculating data
                populateGrid("m", "d");
                progressBar1.Hide();
            }
        }

        private void pm()
        {
            p_and_m_variation(m, d, N, p, L, U, P, max_p, max_m, tests, eps, p_threshold);
        }
        private async void PandMButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxPTextBox.Text != "" && maxMTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => pm());
                //after calculating data
                populateGrid("p", "m");
                progressBar1.Hide();
            }
        }

        private void LTextBox_TextChanged(object sender, EventArgs e)
        {
            if (LTextBox.Text.Length != 0 && int.TryParse(LTextBox.Text, out L))
            {
                L = Convert.ToInt32(LTextBox.Text);
            }
            else
            {
                LTextBox.Text = "";
            }
        }

        private void clearDataButton_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
        }

        private void showDataButton_Click(object sender, EventArgs e)
        {
            populateGrid("col1","col2");
        }

        private void Pd()
        {
            P_and_d_variation(m, d, N, p, L, U, P, max_P, max_d, tests, eps, p_threshold);
        }
        private async void BigPandDbutton_Click_1(object sender, EventArgs e)
        {
            if (checkAll() && maxBigPTextBox.Text != "" && maxDTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Pd());
                //after calculating data
                populateGrid("P", "d");
                progressBar1.Hide();
            }
        }

        private void maxDTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maxDTextBox.Text.Length != 0 && double.TryParse(maxDTextBox.Text, out max_d))
            {
                max_d = Convert.ToDouble(maxDTextBox.Text);
                if (max_d < d)
                {
                    maxDTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxDTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                maxDTextBox.Text = "";
            }
            if (maxDTextBox.Text == "")
            {
                maxDTextBox.BackColor = SystemColors.InactiveCaption;
            }
            
        }

        private void showGraphButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            string csvFile = System.IO.Path.Combine(Application.StartupPath, "data.csv");
            var retainedLines = File.ReadAllLines(csvFile)
                        .Where((x, i) => i == 0 || x.Split(';')[1] != "clear");

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //StreamWriter file = new StreamWriter(saveFileDialog1.FileName.ToString());
                string a = saveFileDialog1.FileName.ToString();
                File.WriteAllLines(a, retainedLines);
                
            }

        }

        private void UN()
        {
            U_and_N_variation(m, d, N, p, L, U, P, max_U, max_N, tests, eps, p_threshold);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxPTextBox.Text != "" && maxDTextBox.Text != "" && maxPTextBox.BackColor!=Color.Red && maxDTextBox.BackColor!=Color.Red)
            {
                PandDbutton.Enabled = true;
            }
            else
            {
                PandDbutton.Enabled = false;
            }
            if (checkAll() && maxUTextBox.Text != "" && maxNTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxUTextBox.BackColor!=Color.Red)
            {
                UandNButton.Enabled = true;
            }
            else
            {
                UandNButton.Enabled = false;
            }
            if (checkAll() && maxUTextBox.Text != "" && maxMTextBox.Text != "" && maxUTextBox.BackColor!=Color.Red && maxMTextBox.BackColor!=Color.Red)
            {
                UandMButton.Enabled = true;
            }
            else
            {
                UandMButton.Enabled = false;
            }
            if (checkAll() && maxNTextBox.Text != "" && maxPTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxPTextBox.BackColor != Color.Red)
            {
                PandNButton.Enabled = true;
            }
            else
            {
                PandNButton.Enabled = false;
            }
            if (checkAll() && maxNTextBox.Text != "" && maxDTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxDTextBox.BackColor != Color.Red)
            {
                MandDButton.Enabled = true;
            }
            else
            {
                MandDButton.Enabled = false;
            }
            if (checkAll() && maxBigPTextBox.Text != "" && maxMTextBox.Text != "" && maxBigPTextBox.BackColor!=Color.Red && maxMTextBox.BackColor!=Color.Red)
            {
                BigPandMButton.Enabled = true;
            }
            else
            {
                BigPandMButton.Enabled = false;
            }
            if (checkAll() && maxMTextBox.Text != "" && maxDTextBox.Text != "" && maxMTextBox.BackColor!=Color.Red && maxDTextBox.BackColor != Color.Red)
            {
                MandDButton.Enabled = true;
            }
            else
            {
                MandDButton.Enabled = false;
            }
            if (checkAll() && maxUTextBox.Text != "" && maxBigPTextBox.Text != "" && maxUTextBox.BackColor!=Color.Red && maxBigPTextBox.BackColor != Color.Red)
            {
                UandBigPButton.Enabled = true;
            }
            else
            {
                UandBigPButton.Enabled = false;
            }
            if (checkAll() && maxUTextBox.Text != "" && maxPTextBox.Text != "" && maxUTextBox.BackColor!=Color.Red && maxPTextBox.BackColor != Color.Red)
            {
                UandPButton.Enabled = true;
            }
            else
            {
                UandPButton.Enabled = false;
            }
            if (checkAll() && maxUTextBox.Text != "" && maxDTextBox.Text != "" && maxUTextBox.BackColor!=Color.Red && maxDTextBox.BackColor != Color.Red)
            {
                UandDButton.Enabled = true;
            }
            else
            {
                UandDButton.Enabled = false;
            }
            if (checkAll() && maxNTextBox.Text != "" && maxBigPTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxBigPTextBox.BackColor != Color.Red)
            {
                NandBigPButton.Enabled = true;
            }
            else
            {
                NandBigPButton.Enabled = false;
            }
            if (checkAll() && maxNTextBox.Text != "" && maxMTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxMTextBox.BackColor != Color.Red)
            {
                NadnMButton.Enabled = true;
            }
            else
            {
                NadnMButton.Enabled = false;
            }
            if (checkAll() && maxBigPTextBox.Text != "" && maxDTextBox.Text != "" && maxBigPTextBox.BackColor!=Color.Red && maxDTextBox.BackColor != Color.Red)
            {
                BigPandDbutton.Enabled = true;
            }
            else
            {
                BigPandDbutton.Enabled = false;
            }
            if (checkAll() && maxPTextBox.Text != "" && maxMTextBox.Text != "" && maxPTextBox.BackColor!=Color.Red && maxMTextBox.BackColor != Color.Red)
            {
                PandMButton.Enabled = true;
            }
            else
            {
                PandMButton.Enabled = false;
            }
            if (checkAll() && maxBigPTextBox.Text != "" && maxPTextBox.Text != "" && maxBigPTextBox.BackColor!=Color.Red && maxPTextBox.BackColor != Color.Red)
            {
                PandPButton.Enabled = true;
            }
            else
            {
                PandPButton.Enabled = false;
            }
            if (checkAll() && maxNTextBox.Text != "" && maxDTextBox.Text != "" && maxNTextBox.BackColor!=Color.Red && maxDTextBox.BackColor != Color.Red)
            {
                NandDButton.Enabled = true;
            }
            else
            {
                NandDButton.Enabled = false;
            }
        }

        private void pd()
        {
            p_and_d_variation(m,d, N, p, L, U,  P, max_p, max_d,  tests, eps, p_threshold);
        }
        private async void PandDbutton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxPTextBox.Text != "" && maxDTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => pd());
                //after calculating data
                populateGrid("p", "d");
                progressBar1.Hide();
            }
        }

        private void GraphButton_Click(object sender, EventArgs e)
        {
            REngine.SetEnvironmentVariables(); // <-- May be omitted; the next line would call it.
            REngine engine = REngine.GetInstance();
            engine.Evaluate("library(plotly)");
            engine.Evaluate("packageVersion('plotly')");
            engine.Evaluate("data=read.csv2(file=\"data.csv\",numerals = \"no.loss\",header=TRUE,colClasses=\"character\")");

            engine.Evaluate("x <- as.double(data[[1]])");
            engine.Evaluate("y <- as.double(data[[2]])");
            engine.Evaluate("z <- as.double(data[[3]])");
            engine.Evaluate("xa<-colnames(data)[1]");
            engine.Evaluate("ya<-colnames(data)[2]");

            engine.Evaluate("p <- plot_ly(x = ~x, y = ~y, z = ~z, type = 'scatter3d') %>% layout(scene = list(xaxis = list(title =xa),yaxis = list(title = ya),zaxis = list(title = 'p(bankruptcy)')))");
            engine.Evaluate("print(p)");

            engine.Evaluate("p1 <- plot_ly(x = ~x, y = ~y, z = ~z, type = 'mesh3d') %>% layout(scene = list(xaxis = list(title =xa),yaxis = list(title = ya),zaxis = list(title = 'p(bankruptcy)')))");
            engine.Evaluate("print(p1)");

            //engine.Evaluate("data2=read.table(\"data.txt\", header=FALSE)");
            //engine.Evaluate("d2<-matrix(as.numeric(unlist(data2)),nrow=nrow(data2))");
            //engine.Evaluate("p2<-plot_ly(z=d2,type='surface')");
            //engine.Evaluate("print(p2)");
            //engine.Evaluate("plot(mydata)");
        }

        private void pThresholdTextBox_TextChanged(object sender, EventArgs e)
        {
            if (pThresholdTextBox.Text.Length != 0 && double.TryParse(pThresholdTextBox.Text, out p_threshold)&& Convert.ToDouble(pThresholdTextBox.Text) <= 1)
            {
                p_threshold = Convert.ToDouble(pThresholdTextBox.Text);
            }
            else
            {
                pThresholdTextBox.Text = "";
            }
        }

        private async void UandNButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxUTextBox.Text != "" && maxNTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => UN());
                //after calculating data
                populateGrid("U", "N");
                progressBar1.Hide();
            }
        }
        private void Up()
        {
            U_and_p_variation(m, d, N, p, L, U, P, max_U, max_p, tests, eps, p_threshold);
        }
        private async void UandPButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxUTextBox.Text != "" && maxPTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Up());
                //after calculating data
                populateGrid("U", "p");
                progressBar1.Hide();
            }

        }

        private void epsTextBox_TextChanged(object sender, EventArgs e)
        {
            if (epsTextBox.Text.Length != 0 && double.TryParse(epsTextBox.Text, out eps)&& Convert.ToDouble(epsTextBox.Text)<=1)
            {
                eps = Convert.ToDouble(epsTextBox.Text);
            }
            else
            {
                epsTextBox.Text = "";
            }
        }

        private void NTextBox_TextChanged(object sender, EventArgs e)
        {
            if (NTextBox.Text.Length != 0 && int.TryParse(NTextBox.Text, out N))
            {
                N = Convert.ToInt32(NTextBox.Text);
                if (max_N < N && maxMTextBox.Text != "")
                {
                    maxNTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxNTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                NTextBox.Text = "";
            }
        }
        private void Np()
        {
            N_and_p_variation(m, d, N, p, L, U, P, max_N, max_p, tests, eps, p_threshold);
        }
        private async void NandpButton_Click(object sender, EventArgs e)
        {
            if (checkAll() && maxNTextBox.Text != "" && maxPTextBox.Text != "")
            {
                progressBar1.Show();
                dataGridView1.DataSource = null;
                await Task.Run(() => Np());
                //after calculating data
                populateGrid("N", "p");
                progressBar1.Hide();
            }
        }

        private void populateGrid(string col1,string col2)
        {
            string csvFile = System.IO.Path.Combine(Application.StartupPath, "data.csv");
            List<string[]> rows = File.ReadAllLines(csvFile).Select(x => x.Split(';')).ToList();
            DataTable dataTable = new DataTable();
            //add cols to datatable:
            dataTable.Columns.Add(col1);
            dataTable.Columns.Add(col2);
            dataTable.Columns.Add("p[b]");
            dataTable.Columns.Add("p[analytic]");

            rows.ForEach(x => { dataTable.Rows.Add(x); });

            dataGridView1.DataSource = dataTable;
            clearDataButton.Show();
            showDataButton.Show();
            showGraphButton.Show();
            GraphButton.Show();
        }

        private void dTextBox_TextChanged(object sender, EventArgs e)
        {
            if (dTextBox.Text.Length != 0 && double.TryParse(dTextBox.Text, out d))
            {
                d = Convert.ToDouble(dTextBox.Text);
                if (max_d < d && maxDTextBox.Text != "")
                {
                    maxDTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxDTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                dTextBox.Text = "";
            }
        }

        private void mTextBox_TextChanged(object sender, EventArgs e)
        {
            if (mTextBox.Text.Length != 0 && double.TryParse(mTextBox.Text, out m))
            {
                m = Convert.ToDouble(mTextBox.Text);
                if (max_m < m && maxMTextBox.Text != "") 
                {
                    maxMTextBox.BackColor = Color.Red;
                }
                else
                {
                    maxMTextBox.BackColor = SystemColors.InactiveCaption;
                }
            }
            else
            {
                mTextBox.Text = "";
            }
        }
        
        private bool checkAll()
        {
            if (mTextBox.Text != "" && dTextBox.Text != "" && pTextBox.Text != "" &&
                NTextBox.Text != "" && UTextBox.Text != "" && bigPTextBox.Text != "" &&
                LTextBox.Text != "" && testsTextBox.Text != "" && epsTextBox.Text != ""
                &&pThresholdTextBox.Text!="")
            {
                return true;
            }
            else return false;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
