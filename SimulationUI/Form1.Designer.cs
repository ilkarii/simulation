﻿namespace SimulationUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.UandPButton = new System.Windows.Forms.Button();
            this.PandNButton = new System.Windows.Forms.Button();
            this.UandNButton = new System.Windows.Forms.Button();
            this.mTextBox = new System.Windows.Forms.TextBox();
            this.dTextBox = new System.Windows.Forms.TextBox();
            this.NTextBox = new System.Windows.Forms.TextBox();
            this.pTextBox = new System.Windows.Forms.TextBox();
            this.LTextBox = new System.Windows.Forms.TextBox();
            this.UTextBox = new System.Windows.Forms.TextBox();
            this.bigPTextBox = new System.Windows.Forms.TextBox();
            this.maxNTextBox = new System.Windows.Forms.TextBox();
            this.maxPTextBox = new System.Windows.Forms.TextBox();
            this.maxUTextBox = new System.Windows.Forms.TextBox();
            this.testsTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.clearDataButton = new System.Windows.Forms.Button();
            this.showDataButton = new System.Windows.Forms.Button();
            this.epsTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.maxMTextBox = new System.Windows.Forms.TextBox();
            this.maxDTextBox = new System.Windows.Forms.TextBox();
            this.maxBigPTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.UandBigPButton = new System.Windows.Forms.Button();
            this.UandMButton = new System.Windows.Forms.Button();
            this.UandDButton = new System.Windows.Forms.Button();
            this.NadnMButton = new System.Windows.Forms.Button();
            this.NandDButton = new System.Windows.Forms.Button();
            this.NandBigPButton = new System.Windows.Forms.Button();
            this.PandPButton = new System.Windows.Forms.Button();
            this.BigPandMButton = new System.Windows.Forms.Button();
            this.BigPandDbutton = new System.Windows.Forms.Button();
            this.MandDButton = new System.Windows.Forms.Button();
            this.PandMButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.simulationPanel = new System.Windows.Forms.Panel();
            this.PandDbutton = new System.Windows.Forms.Button();
            this.dataSetPanel = new System.Windows.Forms.Panel();
            this.GraphButton = new System.Windows.Forms.Button();
            this.showGraphButton = new System.Windows.Forms.Button();
            this.dataHeaderPanel = new System.Windows.Forms.Panel();
            this.simulatonHeaderPaenl = new System.Windows.Forms.Panel();
            this.datasetHeaderPanel = new System.Windows.Forms.Panel();
            this.pThresholdTextBox = new System.Windows.Forms.TextBox();
            this.pThresholdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.dataPanel.SuspendLayout();
            this.simulationPanel.SuspendLayout();
            this.dataSetPanel.SuspendLayout();
            this.dataHeaderPanel.SuspendLayout();
            this.simulatonHeaderPaenl.SuspendLayout();
            this.datasetHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(118, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "m";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(118, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 24);
            this.label2.TabIndex = 11;
            this.label2.Text = "d";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(118, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 24);
            this.label3.TabIndex = 12;
            this.label3.Text = "N";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(119, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 24);
            this.label4.TabIndex = 13;
            this.label4.Text = "p";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(118, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 24);
            this.label5.TabIndex = 14;
            this.label5.Text = "L";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(118, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 24);
            this.label6.TabIndex = 15;
            this.label6.Text = "U";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(118, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 24);
            this.label7.TabIndex = 16;
            this.label7.Text = "P";
            // 
            // UandPButton
            // 
            this.UandPButton.BackColor = System.Drawing.Color.LightBlue;
            this.UandPButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.UandPButton.Enabled = false;
            this.UandPButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UandPButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UandPButton.Location = new System.Drawing.Point(115, 49);
            this.UandPButton.Name = "UandPButton";
            this.UandPButton.Size = new System.Drawing.Size(93, 35);
            this.UandPButton.TabIndex = 20;
            this.UandPButton.Text = "U and p variation";
            this.UandPButton.UseVisualStyleBackColor = false;
            this.UandPButton.Click += new System.EventHandler(this.UandPButton_Click);
            // 
            // PandNButton
            // 
            this.PandNButton.BackColor = System.Drawing.Color.LightBlue;
            this.PandNButton.Enabled = false;
            this.PandNButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PandNButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PandNButton.Location = new System.Drawing.Point(16, 131);
            this.PandNButton.Name = "PandNButton";
            this.PandNButton.Size = new System.Drawing.Size(93, 36);
            this.PandNButton.TabIndex = 21;
            this.PandNButton.Text = "N and p variation";
            this.PandNButton.UseVisualStyleBackColor = false;
            this.PandNButton.Click += new System.EventHandler(this.NandpButton_Click);
            // 
            // UandNButton
            // 
            this.UandNButton.BackColor = System.Drawing.Color.LightBlue;
            this.UandNButton.Enabled = false;
            this.UandNButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UandNButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UandNButton.Location = new System.Drawing.Point(16, 8);
            this.UandNButton.Name = "UandNButton";
            this.UandNButton.Size = new System.Drawing.Size(93, 76);
            this.UandNButton.TabIndex = 22;
            this.UandNButton.Text = "U and N variation";
            this.UandNButton.UseVisualStyleBackColor = false;
            this.UandNButton.Click += new System.EventHandler(this.UandNButton_Click);
            // 
            // mTextBox
            // 
            this.mTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.mTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mTextBox.Location = new System.Drawing.Point(22, 33);
            this.mTextBox.Name = "mTextBox";
            this.mTextBox.Size = new System.Drawing.Size(45, 20);
            this.mTextBox.TabIndex = 23;
            this.mTextBox.TextChanged += new System.EventHandler(this.mTextBox_TextChanged);
            // 
            // dTextBox
            // 
            this.dTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dTextBox.Location = new System.Drawing.Point(22, 58);
            this.dTextBox.Name = "dTextBox";
            this.dTextBox.Size = new System.Drawing.Size(45, 20);
            this.dTextBox.TabIndex = 24;
            this.dTextBox.TextChanged += new System.EventHandler(this.dTextBox_TextChanged);
            // 
            // NTextBox
            // 
            this.NTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.NTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NTextBox.Location = new System.Drawing.Point(22, 111);
            this.NTextBox.Name = "NTextBox";
            this.NTextBox.Size = new System.Drawing.Size(45, 20);
            this.NTextBox.TabIndex = 25;
            this.NTextBox.TextChanged += new System.EventHandler(this.NTextBox_TextChanged);
            // 
            // pTextBox
            // 
            this.pTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTextBox.Location = new System.Drawing.Point(22, 84);
            this.pTextBox.Name = "pTextBox";
            this.pTextBox.Size = new System.Drawing.Size(45, 20);
            this.pTextBox.TabIndex = 26;
            this.pTextBox.TextChanged += new System.EventHandler(this.pTextBox_TextChanged);
            // 
            // LTextBox
            // 
            this.LTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.LTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LTextBox.Location = new System.Drawing.Point(22, 189);
            this.LTextBox.Name = "LTextBox";
            this.LTextBox.Size = new System.Drawing.Size(96, 20);
            this.LTextBox.TabIndex = 27;
            this.LTextBox.TextChanged += new System.EventHandler(this.LTextBox_TextChanged);
            // 
            // UTextBox
            // 
            this.UTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.UTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UTextBox.Location = new System.Drawing.Point(22, 137);
            this.UTextBox.Name = "UTextBox";
            this.UTextBox.Size = new System.Drawing.Size(45, 20);
            this.UTextBox.TabIndex = 28;
            this.UTextBox.TextChanged += new System.EventHandler(this.UTextBox_TextChanged);
            // 
            // bigPTextBox
            // 
            this.bigPTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.bigPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bigPTextBox.Location = new System.Drawing.Point(22, 163);
            this.bigPTextBox.Name = "bigPTextBox";
            this.bigPTextBox.Size = new System.Drawing.Size(45, 20);
            this.bigPTextBox.TabIndex = 29;
            this.bigPTextBox.TextChanged += new System.EventHandler(this.bigPTextBox_TextChanged);
            // 
            // maxNTextBox
            // 
            this.maxNTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxNTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxNTextBox.Location = new System.Drawing.Point(73, 111);
            this.maxNTextBox.Name = "maxNTextBox";
            this.maxNTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxNTextBox.TabIndex = 30;
            this.maxNTextBox.TextChanged += new System.EventHandler(this.maxNTextBox_TextChanged);
            // 
            // maxPTextBox
            // 
            this.maxPTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxPTextBox.Location = new System.Drawing.Point(73, 85);
            this.maxPTextBox.Name = "maxPTextBox";
            this.maxPTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxPTextBox.TabIndex = 31;
            this.maxPTextBox.TextChanged += new System.EventHandler(this.maxPTextBox_TextChanged);
            // 
            // maxUTextBox
            // 
            this.maxUTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxUTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxUTextBox.Location = new System.Drawing.Point(73, 137);
            this.maxUTextBox.Name = "maxUTextBox";
            this.maxUTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxUTextBox.TabIndex = 32;
            this.maxUTextBox.TextChanged += new System.EventHandler(this.maxUTextBox_TextChanged);
            // 
            // testsTextBox
            // 
            this.testsTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.testsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.testsTextBox.Location = new System.Drawing.Point(22, 217);
            this.testsTextBox.Name = "testsTextBox";
            this.testsTextBox.Size = new System.Drawing.Size(96, 20);
            this.testsTextBox.TabIndex = 33;
            this.testsTextBox.TextChanged += new System.EventHandler(this.testsTextBox_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(120, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 24);
            this.label11.TabIndex = 34;
            this.label11.Text = "tests";
            // 
            // clearDataButton
            // 
            this.clearDataButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.clearDataButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearDataButton.Location = new System.Drawing.Point(385, 365);
            this.clearDataButton.Name = "clearDataButton";
            this.clearDataButton.Size = new System.Drawing.Size(82, 35);
            this.clearDataButton.TabIndex = 36;
            this.clearDataButton.Text = "hide data";
            this.clearDataButton.UseVisualStyleBackColor = false;
            this.clearDataButton.Visible = false;
            this.clearDataButton.Click += new System.EventHandler(this.clearDataButton_Click);
            // 
            // showDataButton
            // 
            this.showDataButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.showDataButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showDataButton.Location = new System.Drawing.Point(297, 365);
            this.showDataButton.Name = "showDataButton";
            this.showDataButton.Size = new System.Drawing.Size(82, 35);
            this.showDataButton.TabIndex = 37;
            this.showDataButton.Text = "show data";
            this.showDataButton.UseVisualStyleBackColor = false;
            this.showDataButton.Visible = false;
            this.showDataButton.Click += new System.EventHandler(this.showDataButton_Click);
            // 
            // epsTextBox
            // 
            this.epsTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.epsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.epsTextBox.Location = new System.Drawing.Point(22, 243);
            this.epsTextBox.Name = "epsTextBox";
            this.epsTextBox.Size = new System.Drawing.Size(96, 20);
            this.epsTextBox.TabIndex = 38;
            this.epsTextBox.TextChanged += new System.EventHandler(this.epsTextBox_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(119, 239);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 24);
            this.label12.TabIndex = 39;
            this.label12.Text = "eps";
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.progressBar1.Location = new System.Drawing.Point(21, 339);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(446, 20);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 40;
            this.progressBar1.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(72, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 24);
            this.label13.TabIndex = 41;
            this.label13.Text = "max\r\n";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(56, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 31);
            this.label14.TabIndex = 42;
            this.label14.Text = "Data";
            // 
            // maxMTextBox
            // 
            this.maxMTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxMTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxMTextBox.Location = new System.Drawing.Point(73, 33);
            this.maxMTextBox.Name = "maxMTextBox";
            this.maxMTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxMTextBox.TabIndex = 43;
            this.maxMTextBox.TextChanged += new System.EventHandler(this.maxMTextBox_TextChanged);
            // 
            // maxDTextBox
            // 
            this.maxDTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxDTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxDTextBox.Location = new System.Drawing.Point(73, 59);
            this.maxDTextBox.Name = "maxDTextBox";
            this.maxDTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxDTextBox.TabIndex = 44;
            this.maxDTextBox.TextChanged += new System.EventHandler(this.maxDTextBox_TextChanged);
            // 
            // maxBigPTextBox
            // 
            this.maxBigPTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.maxBigPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxBigPTextBox.Location = new System.Drawing.Point(73, 164);
            this.maxBigPTextBox.Name = "maxBigPTextBox";
            this.maxBigPTextBox.Size = new System.Drawing.Size(45, 20);
            this.maxBigPTextBox.TabIndex = 45;
            this.maxBigPTextBox.TextChanged += new System.EventHandler(this.maxBigPTextBox_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(18, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 24);
            this.label16.TabIndex = 48;
            this.label16.Text = "value";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(48, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(140, 31);
            this.label18.TabIndex = 50;
            this.label18.Text = "Simulation";
            // 
            // UandBigPButton
            // 
            this.UandBigPButton.BackColor = System.Drawing.Color.LightBlue;
            this.UandBigPButton.Enabled = false;
            this.UandBigPButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UandBigPButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UandBigPButton.Location = new System.Drawing.Point(115, 8);
            this.UandBigPButton.Name = "UandBigPButton";
            this.UandBigPButton.Size = new System.Drawing.Size(93, 36);
            this.UandBigPButton.TabIndex = 51;
            this.UandBigPButton.Text = "U and P variation";
            this.UandBigPButton.UseVisualStyleBackColor = false;
            this.UandBigPButton.Click += new System.EventHandler(this.UandBigPButton_Click);
            // 
            // UandMButton
            // 
            this.UandMButton.BackColor = System.Drawing.Color.LightBlue;
            this.UandMButton.Enabled = false;
            this.UandMButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UandMButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UandMButton.Location = new System.Drawing.Point(16, 90);
            this.UandMButton.Name = "UandMButton";
            this.UandMButton.Size = new System.Drawing.Size(93, 35);
            this.UandMButton.TabIndex = 52;
            this.UandMButton.Text = "U and m variation";
            this.UandMButton.UseVisualStyleBackColor = false;
            this.UandMButton.Click += new System.EventHandler(this.UandMButton_Click);
            // 
            // UandDButton
            // 
            this.UandDButton.BackColor = System.Drawing.Color.LightBlue;
            this.UandDButton.Enabled = false;
            this.UandDButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UandDButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UandDButton.Location = new System.Drawing.Point(115, 90);
            this.UandDButton.Name = "UandDButton";
            this.UandDButton.Size = new System.Drawing.Size(93, 35);
            this.UandDButton.TabIndex = 53;
            this.UandDButton.Text = "U and d variation";
            this.UandDButton.UseVisualStyleBackColor = false;
            this.UandDButton.Click += new System.EventHandler(this.UandDButton_Click);
            // 
            // NadnMButton
            // 
            this.NadnMButton.BackColor = System.Drawing.Color.LightBlue;
            this.NadnMButton.Enabled = false;
            this.NadnMButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NadnMButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NadnMButton.Location = new System.Drawing.Point(115, 173);
            this.NadnMButton.Name = "NadnMButton";
            this.NadnMButton.Size = new System.Drawing.Size(93, 35);
            this.NadnMButton.TabIndex = 54;
            this.NadnMButton.Text = "N and m variation";
            this.NadnMButton.UseVisualStyleBackColor = false;
            this.NadnMButton.Click += new System.EventHandler(this.NandMButton_Click);
            // 
            // NandDButton
            // 
            this.NandDButton.BackColor = System.Drawing.Color.LightBlue;
            this.NandDButton.Enabled = false;
            this.NandDButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NandDButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NandDButton.Location = new System.Drawing.Point(16, 173);
            this.NandDButton.Name = "NandDButton";
            this.NandDButton.Size = new System.Drawing.Size(93, 35);
            this.NandDButton.TabIndex = 55;
            this.NandDButton.Text = "N and d Variation";
            this.NandDButton.UseVisualStyleBackColor = false;
            this.NandDButton.Click += new System.EventHandler(this.NandDButton_Click);
            // 
            // NandBigPButton
            // 
            this.NandBigPButton.BackColor = System.Drawing.Color.LightBlue;
            this.NandBigPButton.Enabled = false;
            this.NandBigPButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NandBigPButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NandBigPButton.Location = new System.Drawing.Point(115, 132);
            this.NandBigPButton.Name = "NandBigPButton";
            this.NandBigPButton.Size = new System.Drawing.Size(93, 35);
            this.NandBigPButton.TabIndex = 56;
            this.NandBigPButton.Text = "N and P variation";
            this.NandBigPButton.UseVisualStyleBackColor = false;
            this.NandBigPButton.Click += new System.EventHandler(this.NandBigPButton_Click);
            // 
            // PandPButton
            // 
            this.PandPButton.BackColor = System.Drawing.Color.LightBlue;
            this.PandPButton.Enabled = false;
            this.PandPButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PandPButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PandPButton.Location = new System.Drawing.Point(115, 298);
            this.PandPButton.Name = "PandPButton";
            this.PandPButton.Size = new System.Drawing.Size(93, 35);
            this.PandPButton.TabIndex = 57;
            this.PandPButton.Text = "P and p variation";
            this.PandPButton.UseVisualStyleBackColor = false;
            this.PandPButton.Click += new System.EventHandler(this.PandPButton_Click);
            // 
            // BigPandMButton
            // 
            this.BigPandMButton.BackColor = System.Drawing.Color.LightBlue;
            this.BigPandMButton.Enabled = false;
            this.BigPandMButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BigPandMButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BigPandMButton.Location = new System.Drawing.Point(16, 214);
            this.BigPandMButton.Name = "BigPandMButton";
            this.BigPandMButton.Size = new System.Drawing.Size(93, 36);
            this.BigPandMButton.TabIndex = 58;
            this.BigPandMButton.Text = "P and m variation";
            this.BigPandMButton.UseVisualStyleBackColor = false;
            this.BigPandMButton.Click += new System.EventHandler(this.BigPandMButton_Click);
            // 
            // BigPandDbutton
            // 
            this.BigPandDbutton.BackColor = System.Drawing.Color.LightBlue;
            this.BigPandDbutton.Enabled = false;
            this.BigPandDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BigPandDbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BigPandDbutton.Location = new System.Drawing.Point(115, 214);
            this.BigPandDbutton.Name = "BigPandDbutton";
            this.BigPandDbutton.Size = new System.Drawing.Size(93, 36);
            this.BigPandDbutton.TabIndex = 59;
            this.BigPandDbutton.Text = "P and d variation";
            this.BigPandDbutton.UseVisualStyleBackColor = false;
            this.BigPandDbutton.Click += new System.EventHandler(this.BigPandDbutton_Click_1);
            // 
            // MandDButton
            // 
            this.MandDButton.BackColor = System.Drawing.Color.LightBlue;
            this.MandDButton.Enabled = false;
            this.MandDButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MandDButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MandDButton.Location = new System.Drawing.Point(16, 256);
            this.MandDButton.Name = "MandDButton";
            this.MandDButton.Size = new System.Drawing.Size(93, 36);
            this.MandDButton.TabIndex = 60;
            this.MandDButton.Text = "m and d variation";
            this.MandDButton.UseVisualStyleBackColor = false;
            this.MandDButton.Click += new System.EventHandler(this.MandDButton_Click);
            // 
            // PandMButton
            // 
            this.PandMButton.BackColor = System.Drawing.Color.LightBlue;
            this.PandMButton.Enabled = false;
            this.PandMButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PandMButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PandMButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PandMButton.Location = new System.Drawing.Point(115, 256);
            this.PandMButton.Name = "PandMButton";
            this.PandMButton.Size = new System.Drawing.Size(93, 36);
            this.PandMButton.TabIndex = 61;
            this.PandMButton.Text = "p and m variation";
            this.PandMButton.UseVisualStyleBackColor = false;
            this.PandMButton.Click += new System.EventHandler(this.PandMButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.SkyBlue;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(21, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(446, 327);
            this.dataGridView1.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(191, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 31);
            this.label8.TabIndex = 63;
            this.label8.Text = "Dataset";
            // 
            // dataPanel
            // 
            this.dataPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataPanel.Controls.Add(this.pThresholdLabel);
            this.dataPanel.Controls.Add(this.pThresholdTextBox);
            this.dataPanel.Controls.Add(this.button1);
            this.dataPanel.Controls.Add(this.mTextBox);
            this.dataPanel.Controls.Add(this.maxMTextBox);
            this.dataPanel.Controls.Add(this.label1);
            this.dataPanel.Controls.Add(this.label16);
            this.dataPanel.Controls.Add(this.label13);
            this.dataPanel.Controls.Add(this.dTextBox);
            this.dataPanel.Controls.Add(this.maxDTextBox);
            this.dataPanel.Controls.Add(this.label2);
            this.dataPanel.Controls.Add(this.pTextBox);
            this.dataPanel.Controls.Add(this.maxPTextBox);
            this.dataPanel.Controls.Add(this.label4);
            this.dataPanel.Controls.Add(this.NTextBox);
            this.dataPanel.Controls.Add(this.maxNTextBox);
            this.dataPanel.Controls.Add(this.label3);
            this.dataPanel.Controls.Add(this.UTextBox);
            this.dataPanel.Controls.Add(this.maxBigPTextBox);
            this.dataPanel.Controls.Add(this.maxUTextBox);
            this.dataPanel.Controls.Add(this.epsTextBox);
            this.dataPanel.Controls.Add(this.label12);
            this.dataPanel.Controls.Add(this.label6);
            this.dataPanel.Controls.Add(this.bigPTextBox);
            this.dataPanel.Controls.Add(this.label7);
            this.dataPanel.Controls.Add(this.LTextBox);
            this.dataPanel.Controls.Add(this.label11);
            this.dataPanel.Controls.Add(this.label5);
            this.dataPanel.Controls.Add(this.testsTextBox);
            this.dataPanel.Location = new System.Drawing.Point(12, 74);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(190, 413);
            this.dataPanel.TabIndex = 64;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(22, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 35);
            this.button1.TabIndex = 49;
            this.button1.Text = "submit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // simulationPanel
            // 
            this.simulationPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.simulationPanel.Controls.Add(this.PandDbutton);
            this.simulationPanel.Controls.Add(this.UandNButton);
            this.simulationPanel.Controls.Add(this.BigPandMButton);
            this.simulationPanel.Controls.Add(this.NandBigPButton);
            this.simulationPanel.Controls.Add(this.NadnMButton);
            this.simulationPanel.Controls.Add(this.PandPButton);
            this.simulationPanel.Controls.Add(this.MandDButton);
            this.simulationPanel.Controls.Add(this.PandMButton);
            this.simulationPanel.Controls.Add(this.UandBigPButton);
            this.simulationPanel.Controls.Add(this.UandPButton);
            this.simulationPanel.Controls.Add(this.UandMButton);
            this.simulationPanel.Controls.Add(this.UandDButton);
            this.simulationPanel.Controls.Add(this.BigPandDbutton);
            this.simulationPanel.Controls.Add(this.NandDButton);
            this.simulationPanel.Controls.Add(this.PandNButton);
            this.simulationPanel.Location = new System.Drawing.Point(208, 74);
            this.simulationPanel.Name = "simulationPanel";
            this.simulationPanel.Size = new System.Drawing.Size(228, 413);
            this.simulationPanel.TabIndex = 65;
            // 
            // PandDbutton
            // 
            this.PandDbutton.BackColor = System.Drawing.Color.LightBlue;
            this.PandDbutton.Enabled = false;
            this.PandDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PandDbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PandDbutton.Location = new System.Drawing.Point(16, 298);
            this.PandDbutton.Name = "PandDbutton";
            this.PandDbutton.Size = new System.Drawing.Size(93, 35);
            this.PandDbutton.TabIndex = 62;
            this.PandDbutton.Text = "p and d variation";
            this.PandDbutton.UseVisualStyleBackColor = false;
            this.PandDbutton.Click += new System.EventHandler(this.PandDbutton_Click);
            // 
            // dataSetPanel
            // 
            this.dataSetPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataSetPanel.Controls.Add(this.GraphButton);
            this.dataSetPanel.Controls.Add(this.showGraphButton);
            this.dataSetPanel.Controls.Add(this.dataGridView1);
            this.dataSetPanel.Controls.Add(this.progressBar1);
            this.dataSetPanel.Controls.Add(this.showDataButton);
            this.dataSetPanel.Controls.Add(this.clearDataButton);
            this.dataSetPanel.Location = new System.Drawing.Point(442, 74);
            this.dataSetPanel.Name = "dataSetPanel";
            this.dataSetPanel.Size = new System.Drawing.Size(489, 413);
            this.dataSetPanel.TabIndex = 66;
            // 
            // GraphButton
            // 
            this.GraphButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.GraphButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GraphButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GraphButton.Location = new System.Drawing.Point(21, 365);
            this.GraphButton.Name = "GraphButton";
            this.GraphButton.Size = new System.Drawing.Size(182, 35);
            this.GraphButton.TabIndex = 63;
            this.GraphButton.Text = "Graph";
            this.GraphButton.UseVisualStyleBackColor = false;
            this.GraphButton.Visible = false;
            this.GraphButton.Click += new System.EventHandler(this.GraphButton_Click);
            // 
            // showGraphButton
            // 
            this.showGraphButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.showGraphButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showGraphButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showGraphButton.Location = new System.Drawing.Point(209, 365);
            this.showGraphButton.Name = "showGraphButton";
            this.showGraphButton.Size = new System.Drawing.Size(82, 35);
            this.showGraphButton.TabIndex = 63;
            this.showGraphButton.Text = "save data";
            this.showGraphButton.UseVisualStyleBackColor = false;
            this.showGraphButton.Visible = false;
            this.showGraphButton.Click += new System.EventHandler(this.showGraphButton_Click);
            // 
            // dataHeaderPanel
            // 
            this.dataHeaderPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataHeaderPanel.Controls.Add(this.label14);
            this.dataHeaderPanel.Location = new System.Drawing.Point(12, 12);
            this.dataHeaderPanel.Name = "dataHeaderPanel";
            this.dataHeaderPanel.Size = new System.Drawing.Size(190, 56);
            this.dataHeaderPanel.TabIndex = 67;
            // 
            // simulatonHeaderPaenl
            // 
            this.simulatonHeaderPaenl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.simulatonHeaderPaenl.Controls.Add(this.label18);
            this.simulatonHeaderPaenl.Location = new System.Drawing.Point(208, 12);
            this.simulatonHeaderPaenl.Name = "simulatonHeaderPaenl";
            this.simulatonHeaderPaenl.Size = new System.Drawing.Size(228, 56);
            this.simulatonHeaderPaenl.TabIndex = 68;
            // 
            // datasetHeaderPanel
            // 
            this.datasetHeaderPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.datasetHeaderPanel.Controls.Add(this.label8);
            this.datasetHeaderPanel.Location = new System.Drawing.Point(442, 12);
            this.datasetHeaderPanel.Name = "datasetHeaderPanel";
            this.datasetHeaderPanel.Size = new System.Drawing.Size(489, 55);
            this.datasetHeaderPanel.TabIndex = 69;
            // 
            // pThresholdTextBox
            // 
            this.pThresholdTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pThresholdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pThresholdTextBox.Location = new System.Drawing.Point(22, 270);
            this.pThresholdTextBox.Name = "pThresholdTextBox";
            this.pThresholdTextBox.Size = new System.Drawing.Size(96, 20);
            this.pThresholdTextBox.TabIndex = 50;
            this.pThresholdTextBox.TextChanged += new System.EventHandler(this.pThresholdTextBox_TextChanged);
            // 
            // pThresholdLabel
            // 
            this.pThresholdLabel.AutoSize = true;
            this.pThresholdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pThresholdLabel.Location = new System.Drawing.Point(118, 266);
            this.pThresholdLabel.Name = "pThresholdLabel";
            this.pThresholdLabel.Size = new System.Drawing.Size(67, 24);
            this.pThresholdLabel.TabIndex = 51;
            this.pThresholdLabel.Text = "p_max";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(943, 499);
            this.Controls.Add(this.datasetHeaderPanel);
            this.Controls.Add(this.simulatonHeaderPaenl);
            this.Controls.Add(this.dataHeaderPanel);
            this.Controls.Add(this.dataSetPanel);
            this.Controls.Add(this.simulationPanel);
            this.Controls.Add(this.dataPanel);
            this.Name = "Form1";
            this.Text = "Simulation";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.dataPanel.ResumeLayout(false);
            this.dataPanel.PerformLayout();
            this.simulationPanel.ResumeLayout(false);
            this.dataSetPanel.ResumeLayout(false);
            this.dataHeaderPanel.ResumeLayout(false);
            this.dataHeaderPanel.PerformLayout();
            this.simulatonHeaderPaenl.ResumeLayout(false);
            this.simulatonHeaderPaenl.PerformLayout();
            this.datasetHeaderPanel.ResumeLayout(false);
            this.datasetHeaderPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button UandPButton;
        private System.Windows.Forms.Button PandNButton;
        private System.Windows.Forms.Button UandNButton;
        private System.Windows.Forms.TextBox mTextBox;
        private System.Windows.Forms.TextBox dTextBox;
        private System.Windows.Forms.TextBox NTextBox;
        private System.Windows.Forms.TextBox pTextBox;
        private System.Windows.Forms.TextBox LTextBox;
        private System.Windows.Forms.TextBox UTextBox;
        private System.Windows.Forms.TextBox bigPTextBox;
        private System.Windows.Forms.TextBox maxNTextBox;
        private System.Windows.Forms.TextBox maxPTextBox;
        private System.Windows.Forms.TextBox maxUTextBox;
        private System.Windows.Forms.TextBox testsTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button clearDataButton;
        private System.Windows.Forms.Button showDataButton;
        private System.Windows.Forms.TextBox epsTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox maxMTextBox;
        private System.Windows.Forms.TextBox maxDTextBox;
        private System.Windows.Forms.TextBox maxBigPTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button UandBigPButton;
        private System.Windows.Forms.Button UandMButton;
        private System.Windows.Forms.Button UandDButton;
        private System.Windows.Forms.Button NadnMButton;
        private System.Windows.Forms.Button NandDButton;
        private System.Windows.Forms.Button NandBigPButton;
        private System.Windows.Forms.Button PandPButton;
        private System.Windows.Forms.Button BigPandMButton;
        private System.Windows.Forms.Button BigPandDbutton;
        private System.Windows.Forms.Button MandDButton;
        private System.Windows.Forms.Button PandMButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.Panel simulationPanel;
        private System.Windows.Forms.Panel dataSetPanel;
        private System.Windows.Forms.Panel dataHeaderPanel;
        private System.Windows.Forms.Panel simulatonHeaderPaenl;
        private System.Windows.Forms.Panel datasetHeaderPanel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button showGraphButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button PandDbutton;
        private System.Windows.Forms.Button GraphButton;
        private System.Windows.Forms.Label pThresholdLabel;
        private System.Windows.Forms.TextBox pThresholdTextBox;
    }
}

